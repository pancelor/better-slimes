local action = require "necro.game.system.Action"
local attack = require "necro.game.character.Attack"
local commonSpell = require "necro.game.data.spell.CommonSpell"
local components = require "necro.game.data.Components"
local customEntities = require "necro.game.data.CustomEntities"
local damage = require "necro.game.system.Damage"
local ecs = require "system.game.Entities"
local event = require "necro.event.Event"
local invincibility = require "necro.game.character.Invincibility"
local move = require "necro.game.system.Move"
local object = require "necro.game.object.Object"
local spell = require "necro.game.spell.Spell"
local spellItem = require "necro.game.item.SpellItem"
local voice = require "necro.audio.Voice"

components.register {
  fireballTurret = {
    components.field.int8("beatId",0),
  },
}

event.turn.add("doFireballTurret", {order="aiActions"}, function(ev)
  local dirs = {
    action.Direction.RIGHT,
    action.Direction.UP_RIGHT,
    action.Direction.UP,
    action.Direction.UP_LEFT,
    action.Direction.LEFT,
    action.Direction.DOWN_LEFT,
    action.Direction.DOWN,
    action.Direction.DOWN_RIGHT,
  }
  for turret in ecs.entitiesWithComponents{"betterslimes_fireballTurret"} do
    if turret.character.canAct then
      local cTurret = turret.betterslimes_fireballTurret
      cTurret.beatId=cTurret.beatId+1
      if cTurret.beatId==32 then
        cTurret.beatId=0
      end
      if cTurret.beatId%4==0 then
        spell.cast(turret,"SpellcastFireball",dirs[1+cTurret.beatId/4],false)
        voice.play(turret, "dorianFireball")
      end
    end
  end
end)

-- make EVERY slime a fireball slime
event.entitySchemaLoadNamedEnemy.add("makeSlimesGood", {key="slime"}, function (ev)
  ev.entity.betterslimes_fireballTurret = {}
end)
